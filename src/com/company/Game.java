package com.company;

import java.util.Scanner;

public class Game {
    public Player[] players;
    public Board board;
    public LetterBank letterbank;
    public Scanner sc;

    public Game() {
        this.players = new Player[2];
        this.board = new Board();
        this.letterbank = new LetterBank();
        this.sc = new Scanner(System.in);
    }
    public void makePlayers(){
        //gracz 1
        System.out.println("Wpisz imi\u0119 pierwszego gracza:");
        players[0] = new Player(sc.nextLine(), this);
        System.out.println(players[0].name + ", wpisz swoje has\u0142o:");
        players[0].password = sc.nextLine();
        Main.clearScreen();

        //gracz 2
        System.out.println("Wpisz imi\u0119 drugiego gracza:");
        players[1] = new Player(sc.nextLine(), this);
        System.out.println(players[1].name + ", wpisz swoje has\u0142o:");
        players[1].password = sc.nextLine();
        Main.clearScreen();

    }
    //    public Board board;
//    public LetterBank letterbank;
//    public Scanner sc;
//
//    public Game(Board board, LetterBank letterbank, Scanner sc) {
//        this.board = board;
//        this.letterbank = letterbank;
//        this.sc = sc;
//    }
}
