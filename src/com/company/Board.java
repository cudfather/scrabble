package com.company;

public class Board {

    public char[][] deepBoard = createDeepBoard();
    public char[][] board = createBoard();

    public char[][] createDeepBoard() {
        char tab[][] = new char[15][15];
        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[i].length; j++) {
                tab[i][j] = ' ';
            }
        }
        return tab;
    }

    private char[][] createBoard() {
        String x0 = "   1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 ";
        String x1 = " +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+";
        String a0 = "a|3s|  |  |2l|  |  |  |3s|  |  |  |2l|  |  |3s|";
        String a1 = " +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+";
        String b0 = "b|  |2s|  |  |  |3l|  |  |  |3l|  |  |  |2s|  |";
        String b1 = " +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+";
        String c0 = "c|  |  |2s|  |  |  |2l|  |2l|  |  |  |2s|  |  |";
        String c1 = " +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+";
        String d0 = "d|2l|  |  |2s|  |  |  |2l|  |  |  |2s|  |  |2l|";
        String d1 = " +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+";
        String e0 = "e|  |  |  |  |2s|  |  |  |  |  |2s|  |  |  |  |";
        String e1 = " +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+";
        String f0 = "f|  |3l|  |  |  |3l|  |  |  |3l|  |  |  |3l|  |";
        String f1 = " +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+";
        String g0 = "g|  |  |2l|  |  |  |2l|  |2l|  |  |  |2l|  |  |";
        String g1 = " +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+";
        String h0 = "h|3s|  |  |2l|  |  |  |##|  |  |  |2l|  |  |3s|";
        String h1 = " +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+";
        String i0 = "i|  |  |2l|  |  |  |2l|  |2l|  |  |  |2l|  |  |";
        String i1 = " +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+";
        String j0 = "j|  |3l|  |  |  |3l|  |  |  |3l|  |  |  |3l|  |";
        String j1 = " +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+";
        String k0 = "k|  |  |  |  |2s|  |  |  |  |  |2s|  |  |  |  |";
        String k1 = " +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+";
        String l0 = "l|2l|  |  |2s|  |  |  |2l|  |  |  |2s|  |  |2l|";
        String l1 = " +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+";
        String m0 = "m|  |  |2s|  |  |  |2l|  |2l|  |  |  |2s|  |  |";
        String m1 = " +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+";
        String n0 = "n|  |2s|  |  |  |3l|  |  |  |3l|  |  |  |2s|  |";
        String n1 = " +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+";
        String o0 = "o|3s|  |  |2l|  |  |  |3s|  |  |  |2l|  |  |3s|";
        String o1 = " +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+";

        char[][] tab = new char[32][47];
        tab[0] = x0.toCharArray();
        tab[1] = x1.toCharArray();
        tab[2] = a0.toCharArray();
        tab[3] = a1.toCharArray();
        tab[4] = b0.toCharArray();
        tab[5] = b1.toCharArray();
        tab[6] = c0.toCharArray();
        tab[7] = c1.toCharArray();
        tab[8] = d0.toCharArray();
        tab[9] = d1.toCharArray();
        tab[10] = e0.toCharArray();
        tab[11] = e1.toCharArray();
        tab[12] = f0.toCharArray();
        tab[13] = f1.toCharArray();
        tab[14] = g0.toCharArray();
        tab[15] = g1.toCharArray();
        tab[16] = h0.toCharArray();
        tab[17] = h1.toCharArray();
        tab[18] = i0.toCharArray();
        tab[19] = i1.toCharArray();
        tab[20] = j0.toCharArray();
        tab[21] = j1.toCharArray();
        tab[22] = k0.toCharArray();
        tab[23] = k1.toCharArray();
        tab[24] = l0.toCharArray();
        tab[25] = l1.toCharArray();
        tab[26] = m0.toCharArray();
        tab[27] = m1.toCharArray();
        tab[28] = n0.toCharArray();
        tab[29] = n1.toCharArray();
        tab[30] = o0.toCharArray();
        tab[31] = o1.toCharArray();

        return tab;
    }

    public void update() {
        for (int x = 0; x < deepBoard.length; x++) {
            for (int y = 0; y < deepBoard[x].length; y++) {
                if (deepBoard[x][y] != ' ') {
                    board[2 * x + 2][3 * y + 3] = deepBoard[x][y];
                    board[2 * x + 2][3 * y + 2] = ' ';
                }
//                System.out.println("x="+x+", y="+y);
//                Main.display(board);
            }
        }
    }
    //    public void show(){
//        for (char[] aBoard : board) {
//            for (char anABoard : aBoard) {
//                System.out.print(anABoard);
//            }
//            System.out.println();
//        }
//    }
//
//    /*{
//        {" "," ","1"," ","2"," ","3"," ","4"," ","5"," ","6"," ","7"," ","8"," ","9"," ","10"," ","11"," ","12"," ","13"," ","14"," ","15"," ","16"," ","17"},
//        {"a",},
//        {" ",},
//        {"b",},
//        {" ",},
//        {"c",},
//        {" ",},
//        {"d",},
//        {" ",},
//        {"e",},
//        {" ",},
//        {"f",},
//        {" ",},
//        {"g",},
//        {" ",},
//        {"h","","\u058d"},
//        {" ",},
//        {"i",},
//        {" ",},
//        {"j",},
//        {" ",},
//        {"k",},
//        {" ",},
//        {"l",},
//        {" ",},
//        {"m",},
//        {" ",},
//        {"n",},
//        {" ",},
//        {"o",}
//    };
//    */
}

