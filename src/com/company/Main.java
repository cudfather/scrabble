package com.company;

public class Main {

//LOOK UP
//server socket
//socket
//tablica byte[][] pod spodem Board
//import java.util.Scanner;

//    private static Scanner sc = new Scanner(System.in);
//    private static LetterBank letterbank = new LetterBank();
//    private static Board board = new Board();

    public static Game game = new Game();

    public static void main(String[] args) {
//        int x = 10;
//        int y = 15;
//        System.out.println("x:" + x + ", y: " + y);
//        System.out.format("x: %s, y: %s", x, y);
        clearScreen();
        game.makePlayers();

        if (whoGoesFirst(game.players[0], game.players[1]) == game.players[0]){
            while(game.letterbank.letterbank.length > 0) {
                game.players[0].turn();
                game.players[1].turn();
            }
        }
        else{
            while (game.letterbank.letterbank.length > 0) {
                game.players[1].turn();
                game.players[0].turn();
            }
        }
    }
    public static void clearScreen(){
        for (int i = 0; i < 42; i++) {
            System.out.println();
        }
    }
    public static Player whoGoesFirst(Player player1, Player player2){
        char whoGoesFirst;
        int whoGoesFirstPlayer1;
        int whoGoesFirstPlayer2;
        boolean tied;
        do {
            tied = false;
            System.out.println();
            whoGoesFirst = game.letterbank.randomFromLetterbank();
            whoGoesFirstPlayer1 = points(whoGoesFirst);
            System.out.println(player1.name + " wylosowa\u0142(a) liter\u0119 " + whoGoesFirst + " wart\u0105 " + points(whoGoesFirst) + " pkt");

            whoGoesFirst = game.letterbank.randomFromLetterbank();
            whoGoesFirstPlayer2 = points(whoGoesFirst);
            System.out.println(player2.name + " wylosowa\u0142(a) liter\u0119 " + whoGoesFirst + " wart\u0105 " + points(whoGoesFirst) + " pkt");

            if (whoGoesFirstPlayer1 == whoGoesFirstPlayer2) {
                System.out.println("Remis! Losujecie ponownie.");
                tied = true;
            }
        } while(tied);
        if(whoGoesFirstPlayer1>whoGoesFirstPlayer2)
            System.out.println("Zaczyna " + player1.name + "!");
        else
            System.out.println("Zaczyna " + player2.name + "!");
        //dobieranie 7 plytek
        System.out.println("Gracze dobieraj\u0105 po 7 liter.");
        player1.refillHand();
        player2.refillHand();
        System.out.println("Wci\u015bnij Enter, aby kontynuowa\u0107");
        String redundant = game.sc.nextLine();
        clearScreen();
        return(whoGoesFirstPlayer1>whoGoesFirstPlayer2 ? player1 : player2);
    }
    public static void display(char[][] tab){
        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[i].length; j++) {
                System.out.print(tab[i][j]);
            }
            System.out.println();
        }
    }
    public static void score(Player player) {
        System.out.println(player + " ma " + player.score + " pkt");
    }
    public static int points(char a) {
        switch(a) {
            case 'A':
            case 'E':
            case 'I':
            case 'N':
            case 'O':
            case 'R':
            case 'S':
            case 'W':
            case 'Z':
                return 1;
            case 'C':
            case 'D':
            case 'K':
            case 'L':
            case 'M':
            case 'P':
            case 'T':
            case 'Y':
                return 2;
            case 'B':
            case 'G':
            case 'H':
            case 'J':
            case '\u0141':
            case 'U':
                return 3;
            case '\u0104':
            case '\u0118':
            case 'F':
            case '\u00d3':
            case '\u015a':
            case '\u017b':
                return 5;
            case '\u0106':
                return 6;
            case '\u0143':
                return 7;
            case '\u0179':
                return 9;
            default:
                return 0;
        }
//        /*
//        if(a=='A' || a=='E' || a=='I' || a=='N' || a=='O' || a=='R' || a=='S' || a=='W' || a=='Z')return 1;
//        if(a=='C' || a=='D' || a=='K' || a=='L' || a=='M' || a=='P' || a=='T' || a=='Y')return 2;
//        if(a=='B' || a=='G' || a=='H' || a=='J' || a=='�?' || a=='U')return 3;
//        if(a=='Ą' || a=='Ę' || a=='F' || a=='Ó' || a=='Ś' || a=='Ż')return 5;
//        if(a=='Ć')return 6;
//        if(a=='Ń')return 7;
//        if(a=='Ź')return 9;
//        else return 0;*/
    }
}