package com.company;

public class Player {
    public String name;
    public int score;
    public char[] hand;
    public String password;
    public Game game;

    public Player(String name, Game game) {
        this.name = name;
        this.score = 0;
        this.password = "";
        this.hand = new char[7];
        this.game = game;
    }
    public void show() {
        //System.out.println(this.name + ", " + this.score + " points");
        System.out.format("%s ma %s pkt\n", name, score);
    }
    public void refillHand(){
        for (int i = 0; i < hand.length; i++)
            if (hand[i] == 0)
                hand[i] = game.letterbank.randomFromLetterbankThenRemove();
    }
    public void showHand() {
        for (char aHand : hand) {
            System.out.print(aHand + ", ");
        }
        System.out.println();
    }
    public int calculatePoints(String word){
        int count = 0;
        for (int i = 0; i < word.length(); i++) {
            count += Main.points(word.charAt(i));
        }
        return count;
    }
    public int x(){
        int x=15;
        do {
            System.out.println("Podaj wiersz (a,b,c):");
            String row = game.sc.nextLine();
            switch (row.charAt(0)) {
                case 'a':
                case 'A':
                    x = 0;
                    break;
                case 'b':
                case 'B':
                    x = 1;
                    break;
                case 'c':
                case 'C':
                    x = 2;
                    break;
                case 'd':
                case 'D':
                    x = 3;
                    break;
                case 'e':
                case 'E':
                    x = 4;
                    break;
                case 'f':
                case 'F':
                    x = 5;
                    break;
                case 'g':
                case 'G':
                    x = 6;
                    break;
                case 'h':
                case 'H':
                    x = 7;
                    break;
                case 'i':
                case 'I':
                    x = 8;
                    break;
                case 'j':
                case 'J':
                    x = 9;
                    break;
                case 'k':
                case 'K':
                    x = 10;
                    break;
                case 'l':
                case 'L':
                    x = 11;
                    break;
                case 'm':
                case 'M':
                    x = 12;
                    break;
                case 'n':
                case 'N':
                    x = 13;
                    break;
                case 'o':
                case 'O':
                    x = 14;
                    break;
            }
            if (x>14){
                System.out.println("Nieprawid\u0142owy adres.");
            }
        } while (x>14);

        return x;
    }
    public int y (){
        int y = 15;
        do {
            System.out.println("Podaj kolumn\u0119 (1,2,3):");
            String col = game.sc.nextLine();
            boolean yBad = false;
            try {
                y = Integer.parseInt(col) - 1;
            }
            catch (Exception f){
                yBad = true;
            }
            if (yBad || 0>y || y>14){
                System.out.println("Nieprawid\u0142owy adres.");
            }
        } while (y>14);

        return y;
    }
    public boolean isAdjacentToExistingWord(String word, int x, int y, boolean toTheRight){
        try {
            if ((toTheRight && game.board.deepBoard[x][y - 1] == 0 && game.board.deepBoard[x][y + word.length()] == 0) ||
                    (!toTheRight && game.board.deepBoard[x - 1][y] == 0 && game.board.deepBoard[x + word.length()][y] == 0)) {
                for (int i = 0; i < word.length(); i++) {
                    if (toTheRight && (game.board.deepBoard[x - 1][y] != 0 || game.board.deepBoard[x + 1][y] != 0))
                        return true;
                    if (!toTheRight && (game.board.deepBoard[x][y - 1] != 0 || game.board.deepBoard[x][y + 1] != 0))
                        return true;
                }
            }
        } catch (Exception e){
            System.out.println(e);
        }
        return false;
    }
    public boolean toTheRight(){
        String str;
        do {
            System.out.println("S\u0142owo pisane w d\u00f3\u0142 (d) czy w prawo (p)?");
            str = game.sc.nextLine();
        } while (!str.equals("d") && !str.equals("D") && !str.equals("p") && !str.equals("P"));

        return (!str.equals("d") && !str.equals("D"));
    }
    public boolean isPartOfExistingWord(String word, int x, int y, boolean toTheRight) {
        for (int i = 0; i < word.length(); i++) {
            if (toTheRight && game.board.deepBoard[x][i+y]!='0') return true;
            if (!toTheRight && game.board.deepBoard[i+x][y]!='0') return true;
        }
        return false;
    }
    public char[] repeatedLetters(String word, int x, int y, boolean toTheRight) {
        char[] tab = new char[15];
        for (int i = 0; i < word.length(); i++) {
            if (toTheRight && game.board.deepBoard[x][i+y]!='0')
                tab[i] = game.board.deepBoard[x][i+y];
            if (!toTheRight && game.board.deepBoard[i+x][y]!='0')
                tab[i] = game.board.deepBoard[i+x][y];
        }
        return tab;
    }
    public boolean isInHand(char a) {
        for (char aHand : hand) {
            if (aHand == a)
                return true;
        }
        return false;
    }
    public String word() {
        System.out.println("Podaj s\u0142owo:");
        return game.sc.nextLine();
    }
    public void turn() {
    refillHand();
    String turnChoice;
    int turnChoiceInt;

    while (true) {
        Main.display(game.board.board);
        System.out.println("Trwa Twoja tura, " + this.name + ". Co chcesz zrobi\u0107?\n" +
                "[0] Poka\u017c deep plansz\u0119\n" +
                "[1] Poka\u017c moje litery\n" +
                "[2] Wymie\u0144 litery (Twoja tura zako\u0144czy si\u0119)\n" +
                "[3] U\u0142\u00f3\u017c s\u0142owo (Twoja tura zako\u0144czy si\u0119)\n" +
                "[4] Poka\u017c punktacj\u0119");

        turnChoice = game.sc.nextLine();
        try {
            turnChoiceInt = Integer.parseInt(turnChoice);

            if (turnChoiceInt == 0){
                displayDeepBoard();
            }

            //pokazanie reki
            if (turnChoiceInt == 1) {
                displayHand();
            }
            //wymiana liter
            if (turnChoiceInt == 2) {
                changeLetters();
                break;
            }
            //ulozenie slowa
            if (turnChoiceInt == 3) {
                putWord();
                break;
            }
            //wyswietlenie punktacji
            if (turnChoiceInt == 4) {
                game.players[0].show();
                game.players[1].show();
            }
        } catch (Exception e){
            Main.clearScreen();
            System.out.println(e);
            System.out.println("Wybierz jedn\u0105 z opcji i wpisz \"1\", \"2\" lub \"3\"");
        }
    }
}
    public void displayDeepBoard() {
        Main.clearScreen();
        Main.display(game.board.deepBoard);
        System.out.println();
    }
    public void displayHand() {
        Main.clearScreen();
        showHand();
        System.out.println();
    }
    public void changeLetters() {
        System.out.println("Wpisz litery, kt\u00f3re chcesz wymieni\u0107 (np. \"ABCGHI\")");
        String changeLetters = game.sc.nextLine();
        char[] change = changeLetters.toCharArray();
        for (char aChange : change) {
            for (int j = 0; j < hand.length; j++) {
                if (aChange == hand[j]) {
                    hand[j] = game.letterbank.randomFromLetterbankThenRemove();
                    game.letterbank.addToLetterbank(aChange);
                    break;
                }
            }
        }
        System.out.print("Twoje litery to teraz ");
        showHand();
        System.out.println("Wci\u015bnij Enter, aby zako\u0144czy\u0107 tur\u0119");
        String redundant = game.sc.nextLine();
    }
    public int addPoints(String word, int x, int y, boolean toTheRight) {
        int basicScore = calculatePoints(word);
        for (int i = 0; i < word.length(); i++) {
            if (toTheRight) {
                if (game.board.board[2 * x + 2][3 * y + 2 + 3 * i] == '2' && game.board.board[2 * x + 2][3 * i + 3 + 3 * i] == 'l')
                    basicScore += word.charAt(i);
                if (game.board.board[2 * x + 2][3 * y + 2 + 3 * i] == '3' && game.board.board[2 * x + 2][3 * y + 3 + 3 * i] == 'l')
                    basicScore += word.charAt(i) * 2;
            }
            else {
                if (game.board.board[2 * x + 2 + 2 * i][3 * y + 2] == '2' && game.board.board[2 * x + 2 + 2 * i][3 * i + 3] == 'l')
                    basicScore += word.charAt(i);
                if (game.board.board[2 * x + 2 + 2 * i][3 * y + 2] == '3' && game.board.board[2 * x + 2 + 2 * i][3 * y + 3] == 'l')
                    basicScore += word.charAt(i) * 2;
            }
        }
        for (int i = 0; i < word.length(); i++) {
            if(toTheRight) {
                if (game.board.board[2 * x + 2][3 * y + 2 + 3 * i] == '2' && game.board.board[2 * x + 2][3 * y + 3 + 3 * i] == 's')
                    basicScore *= 2;
                if (game.board.board[2 * x + 2][3 * y + 2 + 3 * i] == '3' && game.board.board[2 * x + 2][3 * y + 3 + 3 * i] == 's')
                    basicScore *= 3;
            }
            else {
                if (game.board.board[2 * x + 2 + 2 * i][3 * y + 2] == '2' && game.board.board[2 * x + 2 + 2 * i][3 * y + 3] == 's')
                    basicScore *= 2;
                if (game.board.board[2 * x + 2 + 2 * i][3 * y + 2] == '3' && game.board.board[2 * x + 2 + 2 * i][3 * y + 3] == 's')
                    basicScore *= 3;
            }
        }
        return basicScore;
    }
    public boolean fitsOnBoardUsedInWorksOnBoard(String word, int x, int y){
        return (x == 7 && word.length() <= 8 - y) || (y == 7 && word.length() <= 8 - x);
    }
    public boolean worksOnBoard(String word, int x, int y, boolean toTheRight){
        return (isPartOfExistingWord(word, x, y, toTheRight) ||
                isAdjacentToExistingWord(word, x, y, toTheRight) ||
                fitsOnBoardUsedInWorksOnBoard(word, x, y));
    }
    public boolean fieldHasNoLetter(int x, int y, int i){
        return  game.board.deepBoard[x][i + y] == ' ' ||
                game.board.deepBoard[x][i + y] == 'l' ||
                game.board.deepBoard[x][i + y] == 's' ||
                game.board.deepBoard[x][i + y] == '#';
    }
    private boolean fieldIsNotSameAsWordLetter(String word, int x, int y, int i){
        return game.board.deepBoard[x][i + y] != word.charAt(i);
    }
    private boolean wordFitsOnBoard(String word, int x, int y, boolean toTheRight){
        return (toTheRight && word.length() <= 15 - y) || (!toTheRight && word.length() <= 15 - x);
    }
    public void putWord() {

        System.out.println("Od kt\u00f3rego pola b\u0119dzie si\u0119 zaczyna\u0142 Tw\u00f3j wyraz (np. a4, h13) ?");
        boolean success = false;
        do {
            int x = x();
            int y = y();
            boolean toTheRight = toTheRight();
            String word = word();
            if (worksOnBoard(word, x, y, toTheRight) && wordFitsOnBoard(word, x, y, toTheRight)) {
                //todo: naprawic ukladanie slow z dwoma takimi samymi literami (uklada sie tylko pierwsza)
                if (toTheRight) {
                    for (int i = 0; i < word.length(); i++) {
                        if ((fieldIsNotSameAsWordLetter(word, x, y, i) &&
                            isInHand(word.charAt(i))) && fieldHasNoLetter(x, y, i)) {
                                game.board.deepBoard[x][i + y] = word.charAt(i);
                                updateHand(word, i, x, y, toTheRight);
                                success = true;
                            }
                        }
                    }
                }
                if (!toTheRight) {
                    for (int i = 0; i < word.length(); i++) {
                        if (fieldIsNotSameAsWordLetter(word, x, y, i) &&
                            isInHand(word.charAt(i)) && fieldHasNoLetter(x, y, i)) {
                                game.board.deepBoard[i + x][y] = word.charAt(i);
                                updateHand(word, i, x, y, toTheRight);
                                success = true;
                        }
                    }
                }

            if (success)
                score += addPoints(word, x, y, toTheRight);
        } while (!success);
        System.out.println("Wci\u015bnij Enter, aby zako\u0144czy\u0107 tur\u0119");
        String redundant = game.sc.nextLine();
        game.board.update();
    }
    public void updateHand(String word, int a, int x, int y, boolean toTheRight){
        char[] tab = repeatedLetters(word, x, y, toTheRight);
            for (int j = 0; j < hand.length; j++) {
                if (word.charAt(a) == hand[j]) {
                    for (int k = 0; k < tab.length; k++) {
                        if (hand[j] == tab[k]) {
                            tab[k] = 0;
                        }
                    }
                        hand[j] = 0;
                }
            }
    }
}

