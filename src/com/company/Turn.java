//package com.company;
//
//public class Turn{
//    public Game game;
//    public Player player;
//
//    public Turn(Game game, Player player) {
//        this.player = player;
//        this.game = game;
//    }
//
//    public void turn() {
//        player.refillHand();
//        String turnChoice;
//        int turnChoiceInt;
//
//        while (true) {
//            Main.display(game.board.board);
//            System.out.println("Trwa Twoja tura, " + player.name + ". Co chcesz zrobi\u0107?\n" +
//                    "[0] Poka\u017c deep plansz\u0119\n" +
//                    "[1] Poka\u017c moje litery\n" +
//                    "[2] Wymie\u0144 litery (Twoja tura zako\u0144czy si\u0119)\n" +
//                    "[3] U\u0142\u00f3\u017c s\u0142owo (Twoja tura zako\u0144czy si\u0119)\n" +
//                    "[4] Poka\u017c punktacj\u0119");
//
//            turnChoice = player.game.sc.nextLine();
//            try {
//                turnChoiceInt = Integer.parseInt(turnChoice);
//
//                if (turnChoiceInt == 0){
//                    displayDeepBoard();
//                }
//
//                //pokazanie reki
//                if (turnChoiceInt == 1) {
//                    displayHand();
//                }
//                //wymiana liter
//                if (turnChoiceInt == 2) {
//                    changeLetters();
//                    break;
//                }
//                //ulozenie slowa
//                if (turnChoiceInt == 3) {
//                    putWord();
//                    break;
//                }
//                //wyswietlenie punktacji
//                if (turnChoiceInt == 4) {
//                    game.players[0].show();
//                    game.players[1].show();
//                }
//            } catch (Exception e){
//                Main.clearScreen();
//                System.out.println(e);
//                System.out.println("Wybierz jedn\u0105 z opcji i wpisz \"1\", \"2\" lub \"3\"");
//            }
//        }
//    }
//    public void displayDeepBoard() {
//        Main.clearScreen();
//        Main.display(game.board.deepBoard);
//        System.out.println();
//    }
//
//    public void displayHand() {
//        Main.clearScreen();
//        player.showHand();
//        System.out.println();
//    }
//
//    public void changeLetters() {
//        System.out.println("Wpisz litery, kt\u00f3re chcesz wymieni\u0107 (np. \"ABCGHI\")");
//        String changeLetters = game.sc.nextLine();
//        char[] change = changeLetters.toCharArray();
//        for (char aChange : change) {
//            for (int j = 0; j < player.hand.length; j++) {
//                if (aChange == player.hand[j]) {
//                    player.hand[j] = game.letterbank.randomFromLetterbankThenRemove();
//                    game.letterbank.addToLetterbank(aChange);
//                    break;
//                }
//            }
//        }
//        System.out.print("Twoje litery to teraz ");
//        player.showHand();
//        System.out.println("Wci\u015bnij Enter, aby zako\u0144czy\u0107 tur\u0119");
//        String redundant = game.sc.nextLine();
//    }
//    public int addPoints(String word, int x, int y, boolean toTheRight) {
//        for (int i = 0; i < 15; i++) {
//            for (int j = 0; j < 15; j++) {
//                if (game.board.board[2 * x + 2][3 * y + 2] == '2' && game.board.board[2 * x + 2][3 * y + 3] == 's')
//                    return (player.calculatePoints(word) * 2);
//                if (game.board.board[2 * x + 2][3 * y + 2] == '3' && game.board.board[2 * x + 2][3 * y + 3] == 's')
//                    return (player.calculatePoints(word) * 3);
//                if (game.board.board[2 * x + 2][3 * y + 2] == '2' && game.board.board[2 * x + 2][3 * y + 3] == 'l')
//                    return (player.calculatePoints(word) + Main.points(word.charAt(toTheRight ? j-y : i-x)));
//                if (game.board.board[2 * x + 2][3 * y + 2] == '3' && game.board.board[2 * x + 2][3 * y + 3] == 'l')
//                    return (player.calculatePoints(word) + Main.points(word.charAt(toTheRight ? j-y : i-x)) * 2);
//            }
//        }
//        return 0;
//    }
//    public void putWord() {
//
//        System.out.println("Od kt\u00f3rego pola b\u0119dzie si\u0119 zaczyna\u0142 Tw\u00f3j wyraz (np. a4, h13) ?");
//        boolean success = false;
//        do {
//            int x = player.x();
//            int y = player.y();
//            boolean toTheRight = player.toTheRight();
//            String word = player.word();
//            if (player.isPartOfExistingWord(word, x, y, toTheRight) ||
//                    player.isAdjacentToExistingWord(word, x, y, toTheRight) ||
//                    (x == 7 && word.length() <= 8 - y) ||
//                    (y == 7 && word.length() <= 8 - x)) {
//                //todo: naprawic ukladanie slow z dwoma takimi samymi literami (uklada sie tylko pierwsza)
//                if (toTheRight && word.length() <= 15 - y) {
//                    for (int i = 0; i < word.length(); i++) {
//                        if (    (   game.board.deepBoard[x][i + y] != word.charAt(i) &&
//                                    player.isInHand(word.charAt(i)))
//                                &&
//                                (   game.board.deepBoard[x][i + y] == ' ' ||
//                                    game.board.deepBoard[x][i + y] == 'l' ||
//                                    game.board.deepBoard[x][i + y] == 's' ||
//                                    game.board.deepBoard[x][i + y] == '#')                     ) {
//                            game.board.deepBoard[x][i + y] = word.charAt(i);
//                            for (int j = 0; j < player.hand.length; j++) {
//                                if (player.hand[j] == word.charAt(i)) {
//                                    player.hand[j] = 0;
//                                    success = true;
//                                    addPoints(word, x, y, toTheRight);
//                                }
//                            }
//                        }
//                    }
//                }
//                if (!toTheRight && word.length() <= 15 - y) {
//                    for (int i = 0; i < word.length(); i++) {
//                        if (    (   game.board.deepBoard[i + x][y] != word.charAt(i) &&
//                                    player.isInHand(word.charAt(i)))
//                                &&
//                                (   game.board.deepBoard[i + x][y] == ' ' ||
//                                    game.board.deepBoard[i + x][y] == 'l' ||
//                                    game.board.deepBoard[i + x][y] == 's' ||
//                                    game.board.deepBoard[i + x][y] == '#')                      ) {
//                            game.board.deepBoard[i + x][y] = word.charAt(i);
//                            for (int j = 0; j < player.hand.length; j++) {
//                                if (player.hand[j] == word.charAt(i)) {
//                                    player.hand[j] = 0;
//                                    success = true;
//                                    addPoints(word, x, y, toTheRight);
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        } while (!success);
//        System.out.println("Wci\u015bnij Enter, aby zako\u0144czy\u0107 tur\u0119");
//        String redundant = game.sc.nextLine();
//        game.board.update();
//    }
//}
