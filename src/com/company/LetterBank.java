package com.company;

import java.util.Arrays;

public class LetterBank {
    public char[] letterbank = {
            'A','A','A','A','A','A','A','A','A',
            'E','E','E','E','E','E','E',
            'I','I','I','I','I','I','I','I',
            'N','N','N','N','N',
            'O','O','O','O','O','O',
            'R','R','R','R',
            'S','S','S','S',
            'W','W','W','W',
            'Z','Z','Z','Z','Z',
            'C','C','C',
            'D','D','D',
            'K','K','K',
            'L','L','L',
            'M','M','M',
            'P','P','P',
            'T','T','T',
            'Y','Y','Y','Y',
            'B','B',
            'G','G',
            'H','H',
            'J','J',
            '\u0141','\u0141',
            'U','U',
            '\u0104',
            '\u0118',
            'F',
            '\u00d3',
            '\u015a',
            '\u017b',
            '\u0106',
            '\u0143',
            '\u0179',
            '?','?'
    };

//    public LetterBank(){
//        letterbank[0] = new Letter('A',1,9);
//        letterbank[1] = new Letter('\u0104',5,1);
//        letterbank[2] = new Letter('B',3,2);
//        letterbank[3] = new Letter('C',2,3);
//        letterbank[4] = new Letter('\u0106',6,1);
//        letterbank[5] = new Letter('D',2,3);
//        letterbank[6] = new Letter('E',1,7);
//        letterbank[7] = new Letter('\u0118',5,1);
//        letterbank[8] = new Letter('F',5,1);
//        letterbank[9] = new Letter('G',3,2);
//        letterbank[10] = new Letter('H',3,2);
//        letterbank[11] = new Letter('I',1,8);
//        letterbank[12] = new Letter('J',3,2);
//        letterbank[13] = new Letter('K',2,3);
//        letterbank[14] = new Letter('L',2,3);
//        letterbank[15] = new Letter('\u0141',3,2);
//        letterbank[16] = new Letter('M',2,3);
//        letterbank[17] = new Letter('N',1,5);
//        letterbank[18] = new Letter('\u0143',7,1);
//        letterbank[19] = new Letter('O',1,6);
//        letterbank[20] = new Letter('\u00d3',5,1);
//        letterbank[21] = new Letter('P',2,3);
//        letterbank[22] = new Letter('R',1,4);
//        letterbank[23] = new Letter('S',1,4);
//        letterbank[24] = new Letter('\u015a',5,1);
//        letterbank[25] = new Letter('T',2,3);
//        letterbank[26] = new Letter('U',3,2);
//        letterbank[27] = new Letter('W',1,4);
//        letterbank[28] = new Letter('Y',2,4);
//        letterbank[29] = new Letter('Z',1,5);
//        letterbank[30] = new Letter('\u0179',9,1);
//        letterbank[31] = new Letter('\u017b',5,1);
//        letterbank[32] = new Letter('?',0,2);
//    }

    public void show(){
        for (char aChar : letterbank) {
            System.out.print(aChar + ", ");
        }
    }
    public char randomFromLetterbank(){
        return letterbank[(int)(Math.random()*letterbank.length)];
    }
    public char randomFromLetterbankThenRemove(){
        int index = (int)(Math.random()*letterbank.length);
        char a = letterbank[index];

        System.arraycopy(letterbank,index+1,letterbank,index,letterbank.length-index-1);
        char[] tab = new char[letterbank.length-1];

        System.arraycopy(letterbank,0,tab,0,tab.length);
        letterbank = Arrays.copyOf(tab, tab.length);
        return a;
    }
    public void addToLetterbank(char a){
        for (int i = 0; i < letterbank.length; i++) {
            if (letterbank[i] == 0){
                letterbank[i] = a;
            }
        }
    }
}
